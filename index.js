const array = [];
let html = "";

// bai 1
function enterNumber() {
    const numberInput = document.querySelector("#number").value * 1;
    document.querySelector("#number").value = "";
    array.push(numberInput);
    html = ` <h4> Chuỗi : ${array}</h4>`;
    document.querySelector("#outputArray").innerHTML = html;
}

function calculatePositiveNumbers() {
    const positiveInteger = array.filter(function (n) {
        return n > 0;
    });
    console.log(array, positiveInteger);
    let sum = 0;
    positiveInteger.forEach(function (value) {
        sum += value;
    });
    console.log(sum);
    html = `<p> Tổng các số dương trong mảng bằng ${sum} </p>`;
    document.querySelector("#bai1").innerHTML = html;
}
// bai 2
function countPositiveIntegersArray() {
    const positiveInteger = array.filter(function (n) {
        return n > 0;
    });
    let count = 0;
    if (positiveInteger.length === 0) {
        html = " Không có số nguyên dương nào";
    } else {
        for (let i = 0; i <= positiveInteger.length; i++) {
            count = i;
        }
        html = ` số nguyên dương trong mảng là ${count}`;
    }
    console.log(html);
    document.querySelector("#bai2").innerHTML = html;
}
// bai 3
function findSmallestNumber() {
    const shiftArray = array.sort(function (a, b) {
        return a - b;
    });
    console.log(shiftArray);
    const numberSmall = shiftArray[0];
    console.log(numberSmall);
    html = `số nhỏ nhất trong mảng là ${numberSmall}`;
    console.log(html);
    document.querySelector("#bai3").innerHTML = html;
}
// bai 4
function smallestPositiveIntegerArray() {
    const positiveInteger = array.filter(function (n) {
        return n > 0;
    });
    const integerNumber = positiveInteger.sort(function (a, b) {
        return a - b;
    });
    const smallIntegerNumber = integerNumber[0];
    html = ` Số nguyên dương nhỏ nhất là ${smallIntegerNumber}`;
    console.log(html);
    document.querySelector("#bai4").innerHTML = html;
}
//  bai 5
function findLastEvenNumber() {
    let numLast = array.findLast((value) => value % 2 === 0);
    if (numLast % 2 === 0) {
        html = ` số chẵn cuối cùng trong mảng là ${numLast}`;
    } else {
        html = ` Số chẵn cuối cuối cùng là 0 trả về -1`;
    }

    document.querySelector("#bai5").innerHTML = html;
}
// bai 6
function change2ValuesArray() {
    const positionChangeFirst =
        document.querySelector("#positionChangeFirst").value * 1;
    const positionChange2nd =
        document.querySelector("#positionChange2nd").value * 1;
    if (positionChangeFirst < 0 || positionChange2nd < 0) {
        html = ` Nhập đúng vị trí`;
    } else {
        let arrayNum1 = array[positionChangeFirst];
        array[positionChangeFirst] = array[positionChange2nd];
        array[positionChange2nd] = arrayNum1;
        // array.forEach(function (value) {});
        console.log(arrayNum1);
        html = ` Hàm thay đảo vị trí ${positionChangeFirst} và vị trí ${positionChange2nd} thành : ${array}`;
    }
    document.querySelector("#bai6").innerHTML = html;
}

//  bài 8

function findPrimeNumber(value) {
    let condition = false;
    if (value < 2) {
        condition = false;
    } else if (value === 2 || value === 3 || value === 5 || value === 7) {
        condition = true;
    } else if (
        value % 2 === 0 ||
        value % 3 === 0 ||
        value % 5 === 0 ||
        value % 7 === 0
    ) {
        condition = false;
    } else {
        condition = true;
    }
    return condition;
}

function outputFirstPrime() {
    const positiveInteger = array.filter(function (n) {
        return n > 0;
    });
    console.log(positiveInteger);
    for (let i = 0; i < positiveInteger.length; i++) {
        if (findPrimeNumber(positiveInteger[i])) {
            html = ` Số nguyên tố đầu tiên là ${positiveInteger[i]}`;
            break;
        } else {
            html = ` không có số nguyên tố nào (-1)`;
        }
    }
    console.log(html);
    document.querySelector("#bai8").innerHTML = html;
}

// bai 7
function sortAscendingOrder() {
    const sortAscen = array.sort(function (a, b) {
        return a - b;
    });
    console.log(sortAscen);
    html = ` thứ tự tăng dần là ${sortAscen}`;
    document.querySelector("#bai7").innerHTML = html;
}

// bài 9
const realNumber = [];
function enterArrayRealNumbers() {
    const numberInput = document.querySelector("#realNumber").value * 1;
    document.querySelector("#realNumber").value = "";
    realNumber.push(numberInput);
    html = ` Chuỗi số thực ${realNumber} `;
    document.querySelector("#array2").innerHTML = html;
    // const arrayNew = array.concat(realNumber);
    // document.querySelector("#arrayNew").innerHTML = html;
}

// const arrayNew = array.concat(realNumber);
// bai 9
function filterArrayIntegers(integerArray) {
    let count = 0;
    for (let i = 0; i < integerArray.length; i++) {
        if (Math.floor(integerArray[i]) === integerArray[i]) {
            count = count + 1;
        }
    }
    return count;
}

function findIntegerArray() {
    // const arrayNew = array.concat(realNumber);
    // const evenNumber = array.filter(function (value) {
    //     return value !== 0;
    // });
    const count = filterArrayIntegers(realNumber);
    console.log(count);
    html = ` số nguyên trong mảng là ${count}`;
    document.querySelector("#bai9").innerHTML = html;
}

// Bài 10

function compareNumberNegativeAndPositiveNumbers() {
    // const arrayNew = array.concat(realNumber);
    const positiveNumbers = array.filter(function (value) {
        return value > 0;
    });
    const negativeNumbers = array.filter(function (value) {
        return value < 0;
    });
    let positive = 0;
    let negative = 0;
    for (let i = 0; i < positiveNumbers.length; i++) {
        positive = i + 1;
    }
    for (let i = 0; i < negativeNumbers.length; i++) {
        negative = i + 1;
    }
    if (positive > negative) {
        html = ` Số dương > Số âm`;
    } else if (positive < negative) {
        html = ` Số âm > số dương`;
    } else {
        html = ` Số âm = số dương`;
    }
    document.querySelector("#bai10").innerHTML = html;
}
